#! /usr/bin/python
lessonCsv = "./timetable.csv"
modules = {'CE111': ('CLAa04', 'LABa02'), 'CE112':('LABa02'), 'CE113':('LABa02'), 'CE123':('LABa03')}

from sys import path, argv
import timetable, lesson

#get the lessons i am taking
timetable = timetable.Timetable(argv[1], lessonCsv, modules)
timetable.printWeek()
