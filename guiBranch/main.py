#!/usr/bin/python

from classes import timetable, lesson
from classes.formats.essex import format

lessonCsv = "/home/webpigeon/Projects/Timetable/guiBranch/classes/timetable.csv"
modules = {'CE111': ('CLAa04', 'LABa02'), 'CE112':('LABa02'), 'CE113':('LABa02'), 'CE123':('LABa03')}

class timetableGen:
    ui = None
    timetable = None
    format = None
    modules = {}

    def __init__(self, modules):
        self.ui = None
        self.timetable = None
        self.format = format(self.modules)

    def main(self, weekNo, filename):
        self.timetable = timetable.Timetable(weekNo, filename, modules)
        self.timetable.setLessons(self.getLessons(filename))
        self.timetable.printWeek()

    def getLessons(self, filename):
        return self.format.getLessons(filename)

MyTimetable = timetableGen(modules)
MyTimetable.main(10, lessonCsv)
