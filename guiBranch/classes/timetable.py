#!/usr/bin/python
from lesson import Lesson

class Timetable:
    lessons = []
    forWeek = 0
    modules = None

    def __init__(self, forWeek, lessonFile, modules):
		"""[Intended week], [Lesson list], [list of classes]
		   intialise a timetable object
		"""
		self.forWeek = int(forWeek)
		self.modules = modules

    def setLessons(self, lessonList):
        self.lessons = lessonList

	#lesson sorting comparison
    def lessonCompaire(self, x, y):
		if x.day > y.day:
			return 1
		elif x.day == y.day:
			if x.start > y.start:
				return 1
			elif x.start == y.start:
				return 0
			elif x.start < y.start:
				return -1
		elif x.day < y.day:
			return -1

    def returnWeek(self, weekID):
		"""
		returns the week sepesified"""
		self.lessons.sort(self.lessonCompaire)
		weeksLessons = []
		if weekID == None:
			weekID = self.forWeek
		else:
			weekID = int(weekID)
		for lesson in self.lessons:
			if lesson.inWeek(weekID):
				#lesson is this week
				weeksLessons.append(lesson)
		return weeksLessons
			

    def printWeek(self, weekID=None):
		"""
		Prints the timetable to stdout"""
		weeksLessons = self.returnWeek(weekID)
		self.__printHeadings()
		for lesson in weeksLessons:
			#The lesson is in this week
			self.__printLesson(lesson)
		print "--------------------------------------------------------------------------------------------------"

    def __printHeadings(self):
		"""[Lesson to be printed]
		prints a lesson to stdout"""
		print "--------------------------------------------------------------------------------------------------"
		print "|| Module | Type\t | Group\t | Day\t | Start\t | End Time\t | Room Name\t||"
		print "--------------------------------------------------------------------------------------------------"

    def __printLesson(self, lesson):
		"""[Lesson to be printed]
		prints a lesson to stdout"""
		if lesson.group == "":
			group = "ALL Groups"
		else:
			group = lesson.group
		print "|| "+lesson.module+" | "+lesson.lType+"\t\t | "+group+"\t | "+str(lesson.day)+"\t | "+lesson.start+"\t | "+lesson.end+"\t | "+lesson.room+" \t||"
