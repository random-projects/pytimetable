#!/usr/bin/python
from csv import reader

class format:
    name = "Essex University Notation"
    types = {'LEC':'Lecture', 'LAB':'Lab', 'CLA':'Class'}
    modules = {}

    def __init__(self, modules):
        self.modules = modules

    def getLessons(self, filename):
        """
            gets the list of lessons and puts them into lesson class objects"""
        lessonList = reader(open(filename))
        lessons = []
        for lessonItem in lessonList:
            if self.takingClass(lessonItem):
                lessons.append(Lesson(lessonItem[0], lessonItem[2], lessonItem[1], lessonItem[4], lessonItem[5], lessonItem[6], lessonItem[7], lessonItem[9]))
        return lessons

    def takingClass(self, lessonItem):
        if lessonItem[0] in self.modules.keys():
            if (lessonItem[1] == ""):
                return True
            elif (lessonItem[1] in self.modules[lessonItem[0]]):
                return True
        return False
