#!/usr/bin/python
class Lesson:
	module 	= None
	group 	= None
	day	= None
	start	= None
	end	= None
	weeks	= [] #A list of weeks this lesson runs
	room	= None
	lType	= None
	dayDict = {'Mon': 1, 'Tue': 2, 'Wed': 3, 'Thu': 4, 'Fri': 5}

	def __init__(self, module, ltype, group, day, start, end, weeks, room):
		"""Lets create a new lesson object"""
		self.module = module
		self.lType = ltype
		self.group = group
		self.day = self.dayDict[day]
		self.start = start
		self.end = end
		self.weeks = self.weekExpand(weeks)
		self.room = room

	def __str__(self):
		return self.module+" | "+str(self.day)+" | "+self.start+" | "+self.group+" | "+str(self.weeks)

	def weekExpand(self, weeks):
		"""[Essex formated week string]
		Expands essex's bloody week format to something which i can run comparisons on"""
		weekList = []
		weekArr = weeks.split(',')
		for newWeeks in weekArr:
			rangeWeeks = newWeeks.split('-')
			if len(rangeWeeks) > 1:
				#This was a range of weeks
				weekList.extend(range(int(rangeWeeks[0]), int(rangeWeeks[1])+1))
			else:
				#This was a single week
				weekList.append(int(rangeWeeks[0]))
		return weekList

	def inWeek(self, week):
		week = int(week)
		return week in self.weeks
