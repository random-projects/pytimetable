#!/usr/bin/python
from csv import reader
from lesson import Lesson

class Timetable:
	lessons = []
	forWeek = 0
	modules = None

	def __init__(self, forWeek, lessonFile, modules):
		"""[Intended week], [Lesson list], [list of classes]
		   intialise a timetable object
		"""
		self.forWeek = int(forWeek)
		self.modules = modules
		self.getLessons(lessonFile)

	def getLessons(self, filename):
		"""
		gets the list of lessons and puts them into lesson class objects"""
		lessonList = reader(open(filename))
		for lessonItem in lessonList:
			if lessonItem[0] in self.modules.keys():
				#I am taking this module
				if (lessonItem[1] == "") or (lessonItem[1] in self.modules[lessonItem[0]]):
					#This lesson is for all students taking this module, or it's for a class i am in
					self.lessons.append(Lesson(lessonItem[0], lessonItem[2], lessonItem[1], lessonItem[4], lessonItem[5], lessonItem[6], lessonItem[7], lessonItem[9]))

	#FIXME
	def conflictCheck(self, lesson1Weeks, lesson2Weeks):
		for week in lesson2Weeks:
			if week in lesson1Weeks:
				#We have a conflict!
				print "Conflict! >.<"
				return True
			else:
				return False

	#lesson sorting comparison
	def lessonCompaire(self, x, y):
		if x.day > y.day:
			return 1
		elif x.day == y.day:
			if x.start > y.start:
				return 1
			elif x.start == y.start:
				#Risk of conflict, check!
				if self.conflictCheck(x.weeks, y.weeks):
					print x
					print y
				return 0
			elif x.start < y.start:
				return -1
		elif x.day < y.day:
			return -1

	def returnWeek(self, weekID):
		"""
		returns the week sepesified"""
		self.lessons.sort(self.lessonCompaire)
		weeksLessons = []
		if weekID == None:
			weekID = self.forWeek
		else:
			weekID = int(weekID)
		for lesson in self.lessons:
			if lesson.inWeek(weekID):
				#lesson is this week
				weeksLessons.append(lesson)
		return weeksLessons
			

	def printWeek(self, weekID=None):
		"""
		Prints the timetable to stdout"""
		weeksLessons = self.returnWeek(weekID)
		self.__printHeadings()
		for lesson in weeksLessons:
			#The lesson is in this week
			self.__printLesson(lesson)
		print "--------------------------------------------------------------------------------------------------"

	def __printHeadings(self):
		"""[Lesson to be printed]
		prints a lesson to stdout"""
		print "--------------------------------------------------------------------------------------------------"
		print "|| Module | Type\t | GROUP\t | Day\t | Start\t | End Time\t | Room Name\t||"
		print "--------------------------------------------------------------------------------------------------"

	def __printLesson(self, lesson):
		"""[Lesson to be printed]
		prints a lesson to stdout"""
		if lesson.group == "":
			group = "ALL Groups"
		else:
			group = lesson.group
		print "|| "+lesson.module+" | "+lesson.lType+"\t\t | "+group+"\t | "+str(lesson.day)+"\t | "+lesson.start+"\t | "+lesson.end+"\t | "+lesson.room+" \t||"
